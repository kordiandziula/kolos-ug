﻿using System;
using System.Collections.Generic;

interface IOrganizationComponent
{
    void Add(IOrganizationComponent component);
    void Remove(IOrganizationComponent component);
    void Display(int depth);
}

class Employee : IOrganizationComponent
{
    private string name;
    private string position;

    public Employee(string name, string position)
    {
        this.name = name;
        this.position = position;
    }

    public void Add(IOrganizationComponent component)
    {
        Console.WriteLine("Nie można zastosować dla pracownika.");
    }

    public void Remove(IOrganizationComponent component)
    {
        Console.WriteLine("Nie można zastosować dla pracownika.");
    }

    public void Display(int depth)
    {
        Console.WriteLine(new string('-', depth) + " " + position + ":" + " " + name);
    }
}

class Team : IOrganizationComponent
{
    private List<IOrganizationComponent> components = new List<IOrganizationComponent>();
    private string name;

    public Team(string name)
    {
        this.name = name;
    }

    public void Add(IOrganizationComponent component)
    {
        components.Add(component);
    }

    public void Remove(IOrganizationComponent component)
    {
        components.Remove(component);
    }

    public void Display(int depth)
    {
        Console.WriteLine(new string('-', depth) + " Zespol: " + " " + name);

        foreach (IOrganizationComponent component in components)
        {
            component.Display(depth + 2);
        }
    }
}

class Program
{
    static void Main(string[] args)
    {
        Team itTeam = new Team("IT");
        itTeam.Add(new Employee("Jan IT Nowak", "Analityk"));
        itTeam.Add(new Employee("Jan IT Kowalski", "Programista"));

        Team marketingTeam = new Team("Marketing");
        marketingTeam.Add(new Employee("Jan Nowak", "Kierownik"));
        marketingTeam.Add(new Employee("Jan Kowalski", "Specjalista"));

        Team company = new Team("ABC");
        company.Add(itTeam);
        company.Add(marketingTeam);


        company.Display(0);

        Console.ReadKey();
    }
}